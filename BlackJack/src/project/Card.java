/*****************************************************************************
 *
 *  Project :  Assignment - A09 Team Project
 *  File    :  Card.java
 *  Name    :  Dante, Mick, Clifford
 *  Date    :  4/18/15
 *
 *  Description:  
 *
 *
 *  Changes : 
 *
 ****************************************************************************/


package project;

import javax.swing.*;


/**
 * Card class
 * @author Dante, Mick, Clifford
 *
 */
public class Card extends JLabel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9067325588578610570L;

	public enum Suit {
		HEARTS, DIAMONDS, SPADES, CLUBS}

	public enum Face
	{
		Ace(11), Two(2), Three(3), Four(4), Five(5), Six(6), Seven(7),
		Eight(8), Nine(9), Ten(10), Jack(10), Queen(10), King(10);

		private int value;

		private Face(int value)
		{
			this.value = value;
		}

		public int getValue()
		{
			return value;
		}

	}
	
	private final Suit suit;
	private final Face face;
	private ImageIcon icon;
	
	/*
	 * 
	 * I took a look at Card.java and have some suggestions here.

Suite can have value: 0.0 to 0.3 for example. This will make evaluation a lot easier.
Enum can have public fields (constructor is private, all vars are final)
I don�t think you guys need loadImages method.
And everything should start from 0, trust me, programmers love 0.
Name the cards with an easy format. like: 2_0.1.png, so that  in Card constructor you can use this:
this.icon = new ImageIcon(getClass().getResource(Face.value + �_� + Suite.value + ".png");

When ever you need just call getImage.


	 * QUESTION - if we named all cards as {1.png, 2.png, 3.png .... 52.png} and used a
	 * 
	 * ArrayList[]+1 (1 to fill the array spot of 0 for simplicity) and used a 
	 * 
	 * random nextInt(52) to draw random cards then checked the object to see isEqual be easier?
	 * 
	 *  I'm just having trouble grasping how this is going to work.  
	 *  
	 *  
	 *  private ArrayList<ImageIcon> deck = new ArrayList<ImageIcon>();
	 *  
	 *  public void loadImages() 
	 *  {
	 *  for ( int i = 1; i <= 52; i++)
	 *  {
	 *	deck.add(createImageIcon("images/i.png", " "));
     *
     *	}
     *	Collections.shuffle(deck);
     *  }
     *  
     *  then we'd use the draw method in Deck.java but instead of the index being 0 and then running
     *  
     *  the shuffle we set the draw to get a random index location inside of the ArrayList<> deck.
     *  
     *   Then we'd only need to check the current card against previously played cards with an isEqual()
     *  
     *   The way we have now might work great btw, idk.  If it does the I'm all for it, but would you guys
     *   
     *   mind helping me understand how it's working:)
     *
	 */
	
	Card(Suit suit, Face face)
	{
		this.face = face;
		this.suit = suit;
		this.icon = new ImageIcon(getClass().getResource("Images/" + this.toString() + ".png"));
		
	}

	public Face getFace()
	{
		return face;
	}
	
	public Suit getSuit()
	{
		return suit;
	}
	
	public ImageIcon getIcon()
	{
		return icon;
	}
	
	public int getValue()
	{
		return this.getFace().getValue();
	}

	@Override
	public String toString()
	{
		return "" + suit + face;
	}
	
	
}
